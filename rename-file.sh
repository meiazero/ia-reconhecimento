#!/bin/bash

if [ $# -eq 0 ]; then
  echo "Por favor, forneça o diretório como argumento."
  exit 1
fi

directory="$1"

if [ ! -d "$directory" ]; then
  echo "O diretório especificado não existe."
  exit 1
fi

cd "$directory" || exit 1

for file in *; do
  new_name=$(echo "$file" | awk '{gsub("_", "-"); print}' | tr '[:upper:]' '[:lower:]')
  if [ "$file" != "$new_name" ]; then
    mv "$file" "$new_name"
    echo "Renomeado: $file -> $new_name"
  fi
done

