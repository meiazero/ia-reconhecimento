import torch
import detectron2
from detectron2.data.datasets import register_coco_instances
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor

# Register the dataset
register_coco_instances('rostos_dataset_test', {}, './dataset/rostos.json', './dataset/test/')

# Load the config
cfg = get_cfg()
cfg.merge_from_file("./detectron2/configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")

# Load the model
model = torch.load('model.pth')
cfg.MODEL.WEIGHTS = model

# Set up the predictor and make predictions
predictor = DefaultPredictor(cfg)
outputs = predictor(im)

